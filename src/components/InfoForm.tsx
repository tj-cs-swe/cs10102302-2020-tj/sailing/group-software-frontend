import React from "react";
import { message, Form, Input, Button } from "antd";

export interface UserInfo {
    username: string;
    password: string;
    nickname: string;
    sex: string;
    phonenum: string;
    email: string;
}

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

interface InfoFormProps {
    onSuccess(info: string): void;
    defaultValues: UserInfo;
}

const InfoForm = (props: InfoFormProps) => {
    const [form] = Form.useForm();

    const onFinish = (values: any) => {
        const login_request = new Request(`user/updateInfo`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify({
                username: props.defaultValues.username,
                passwrod: props.defaultValues.password,
                ...values,
            }),
            cache: "reload",
        });
        fetch(login_request)
            .then(
                (response: Response) => {
                    return response.text();
                },
                (reason: any) => {
                    message.error("更新失败：网络异常！");
                    console.log(reason);
                }
            )
            .then(
                (response: any) => {
                    console.log("更新成功" + response);
                    props.onSuccess(response);
                },
                (reason: any) => {
                    message.error("更新失败：服务器异常！");
                    console.log(reason);
                }
            );
    };

    return (
        <Form
            {...formItemLayout}
            form={form}
            name="register"
            onFinish={onFinish}
            scrollToFirstError
        >
            <Form.Item
                name="nickname"
                label="昵称"
                rules={[
                    {
                        required: true,
                        message: "请输入您的昵称!",
                        whitespace: true,
                    },
                ]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                name="sex"
                label="性别"
                rules={[
                    {
                        required: true,
                        message: "请输入您的性别！",
                    },
                ]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                name="phonenum"
                label="电话号码"
                rules={[
                    {
                        required: true,
                        message: "请输入您的电话号码!",
                    },
                ]}
            >
                <Input style={{ width: "100%" }} />
            </Form.Item>

            <Form.Item
                name="email"
                label="电子邮箱"
                rules={[
                    {
                        type: "email",
                        message: "请输入有效的电子邮箱地址！",
                    },
                    {
                        required: true,
                        message: "请输入您的电子邮箱!",
                    },
                ]}
            >
                <Input />
            </Form.Item>

            <Form.Item {...tailFormItemLayout}>
                <Button type="primary" htmlType="submit">
                    修改
                </Button>
            </Form.Item>
        </Form>
    );
};

export default InfoForm;
