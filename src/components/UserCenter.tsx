import * as React from "react";
import { message, List, Layout, Menu } from "antd";
import { ClickParam } from "antd/es/menu";

import InfoForm, { UserInfo } from "./InfoForm";

const { Sider, Content } = Layout;

interface UserCenterProps {
    onLogout(): void;
}

interface UserCenterStates {
    current: string;
    userinfo: UserInfo;
}

async function getinfo(): Promise<UserInfo> {
    const user_request = new Request("/user/myInfo");
    const response = await fetch(user_request);
    return response.json();
}

class UserCenter extends React.Component<UserCenterProps, UserCenterStates> {
    constructor(props: UserCenterProps) {
        super(props);
        let info: UserInfo = {
            username: "null",
            password: "null",
            nickname: "null",
            sex: "null",
            phonenum: "null",
            email: "null",
        };
        this.state = {
            current: "1",
            userinfo: info,
        };
        getinfo().then((userinfo: UserInfo) => {
            info = userinfo;
            this.setState({
                userinfo: userinfo
            });
        });
    }

    handleClick(clickEvent: ClickParam) {
        if (clickEvent.key === "1") {
            getinfo().then((userinfo: UserInfo) => {
                this.setState({
                    current: "1",
                    userinfo: userinfo
                });
            });
        } else if (clickEvent.key === "2") {
            this.setState({
                current: "2",
            });
        } else {
            const logout_request = new Request("/user/loginOut");
            fetch(logout_request);
            document.cookie = "";
            this.props.onLogout();
        }
    }

    userinfo(): React.ReactNode {
        const data = [
            {
                title: "用户名",
                value: this.state.userinfo.username,
            },
            {
                title: "昵称",
                value: this.state.userinfo.nickname,
            },
            {
                title: "性别",
                value: this.state.userinfo.sex,
            },
            {
                title: "电话号码",
                value: this.state.userinfo.phonenum,
            },
            {
                title: "电子邮箱",
                value: this.state.userinfo.email,
            },
        ];
        return (
            <List
                itemLayout="horizontal"
                dataSource={data}
                renderItem={(item: any) => (
                    <List.Item>
                        <List.Item.Meta
                            title={item.title}
                            description={item.value}
                        />
                    </List.Item>
                )}
            />
        );
    }

    render(): React.ReactNode {
        const content =
            this.state.current === "1" ? (
                this.userinfo()
            ) : (
                <InfoForm
                    onSuccess={(info: string) => {
                        message.info(info);
                        getinfo().then((userinfo: UserInfo) => {
                            this.setState({
                                current: "1",
                                userinfo: userinfo
                            });
                        });
                    }}
                    defaultValues={this.state.userinfo}
                />
            );
        return (
            <Layout>
                <Sider>
                    <Menu
                        className="vertical-menu"
                        mode="inline"
                        defaultSelectedKeys={["1"]}
                        onClick={(clickEvent: ClickParam) => {
                            this.handleClick(clickEvent);
                        }}
                        style={{ height: "100%" }}
                    >
                        <Menu.Item key="1">用户信息</Menu.Item>
                        <Menu.Item key="2">编辑信息</Menu.Item>
                        <Menu.Item key="3">注销</Menu.Item>
                    </Menu>
                </Sider>
                <Layout>
                    <Content>{content}</Content>
                </Layout>
            </Layout>
        );
    }
}

export default UserCenter;
