import React from "react";
import { message, Form, Input, Button } from "antd";

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

interface RegistrationFormProps {
    onRegisteration(): void;
}

const RegistrationForm = (props: RegistrationFormProps) => {
    const [form] = Form.useForm();

    const onFinish = (values: any) => {
        const login_request = new Request(
            `user/register`,
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({
                    username: values.username,
                    password: values.password,
                    nickname: values.nickname,
                    sex: values.sex,
                    phonenum: values.phonenum,
                    email: values.email
                }),
                cache: "reload",
            }
        );
        fetch(login_request)
            .then(
                (response: Response) => {
                    return response.json();
                },
                (reason: any) => {
                    message.error("注册失败：网络异常！");
                    console.log(reason);
                }
            )
            .then(
                (response: any) => {
                    if (response.status === false) {
                        message.error(`注册失败： ${response.msg}`);
                    } else {
                        console.log("注册成功" + response);
                        props.onRegisteration();
                    }
                },
                (reason: any) => {
                    message.error("注册失败：服务器异常！");
                    console.log(reason);
                }
            );
    };

    return (
        <Form
            {...formItemLayout}
            form={form}
            name="register"
            onFinish={onFinish}
            scrollToFirstError
        >
            <Form.Item
                name="username"
                label="用户名"
                rules={[
                    {
                        required: true,
                        message: "请输入您的用户名！",
                        whitespace: true,
                    },
                ]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                name="password"
                label="密码"
                rules={[
                    {
                        required: true,
                        message: "请输入您的密码",
                    },
                ]}
                hasFeedback
            >
                <Input.Password />
            </Form.Item>

            <Form.Item
                name="confirm"
                label="确认密码"
                dependencies={["password"]}
                hasFeedback
                rules={[
                    {
                        required: true,
                        message: "请确认您的密码",
                    },
                    ({ getFieldValue }) => ({
                        validator(rule, value) {
                            if (!value || getFieldValue("password") === value) {
                                return Promise.resolve();
                            }
                            return Promise.reject("您输入的两次密码必须一致！");
                        },
                    }),
                ]}
            >
                <Input.Password />
            </Form.Item>

            <Form.Item
                name="nickname"
                label="昵称"
                rules={[
                    {
                        required: true,
                        message: "请输入您的昵称!",
                        whitespace: true,
                    },
                ]}
            >
                <Input />
            </Form.Item>
            
            <Form.Item
                name="sex"
                label="性别"
                rules={[
                    {
                        required: true,
                        message: "请输入您的性别！",
                    },
                ]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                name="phonenum"
                label="电话号码"
                rules={[
                    {
                        required: true,
                        message: "请输入您的电话号码!",
                    },
                ]}
            >
                <Input style={{ width: "100%" }} />
            </Form.Item>

            <Form.Item
                name="email"
                label="电子邮箱"
                rules={[
                    {
                        type: "email",
                        message: "请输入有效的电子邮箱地址！",
                    },
                    {
                        required: true,
                        message: "请输入您的电子邮箱!",
                    },
                ]}
            >
                <Input />
            </Form.Item>

            <Form.Item {...tailFormItemLayout}>
                <Button type="primary" htmlType="submit">
                    注册
                </Button>
            </Form.Item>
        </Form>
    );
};

export default RegistrationForm;
