import * as React from "react";
import { Layout, Menu, Row } from "antd";
import { ClickParam } from "antd/es/menu";

import * as Cookie from "./components/Cookie";
import LoginForm from "./components/LoginForm";
import RegistrationForm from "./components/RegisterationForm";
import UserCenter from "./components/UserCenter";
import "./App.css";

const { Header, Content } = Layout;

interface AppState {
    login: boolean;
    username: string;
    current: string;
}

class App extends React.Component<{}, AppState> {
    constructor(props: {}) {
        super(props);
        let username = Cookie.getCookie("username");
        if (username === "") {
            this.state = {
                login: false,
                username: "",
                current: "1",
            };
        } else {
            this.state = {
                login: true,
                username: username,
                current: "1",
            };
        }
    }

    handleClick(e: ClickParam) {
        this.setState({
            current: e.key,
        });
    }

    handleLogin(username: string) {
        Cookie.setCookie('username', username, 1);
        this.setState({
            login: true,
            username: username,
            current: "1",
        });
    }

    handleRegisteration() {
        this.setState({
            login: false,
            current: "1",
        })
    }

    handleLogout() {
        Cookie.clearCookie('username');
        this.setState({
            login: false,
            username: "",
            current: "1",
        });
    }

    render() {
        if (!this.state.login) {
            const content =
                this.state.current === "1" ? (
                    <LoginForm
                        onLogin={(username: string) => {
                            this.handleLogin(username);
                        }}
                    />
                ) : (
                    <RegistrationForm
                        onRegisteration={() => {
                            this.handleRegisteration();
                        }}
                    />
                );
            return (
                <Layout style={{ height: "100vh" }}>
                    <Header className="header">
                        <div className="logo" />
                        <Menu
                            theme="dark"
                            mode="horizontal"
                            defaultSelectedKeys={["1"]}
                            onClick={(e: ClickParam) => {
                                this.handleClick(e);
                            }}
                        >
                            <Menu.Item key="1">登录</Menu.Item>
                            <Menu.Item key="2">注册</Menu.Item>
                        </Menu>
                    </Header>
                    <Content>
                        <Row
                            align="middle"
                            justify="center"
                            style={{ height: "100vh" }}
                        >
                            {content}
                        </Row>
                    </Content>
                </Layout>
            );
        } else {
            const content = (
                <UserCenter
                    onLogout={() => {
                        this.handleLogout();
                    }}
                />
            );
            return (
                <Layout style={{ height: "100vh" }}>
                    <Header className="header">
                        <div className="logo" />
                        <Menu
                            theme="dark"
                            mode="horizontal"
                            defaultSelectedKeys={["3"]}
                        >
                            <Menu.Item key="1">主页</Menu.Item>
                            <Menu.Item key="2">搜索</Menu.Item>
                            <Menu.Item key="3">个人中心</Menu.Item>
                        </Menu>
                    </Header>
                    <Content style={{ height: "100vh" }}>{content}</Content>
                </Layout>
            );
        }
    }
}

export default App;
